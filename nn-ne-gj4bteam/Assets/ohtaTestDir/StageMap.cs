﻿using UnityEngine;
using System.Collections;

public class StageMap : MonoBehaviour {

	[SerializeField] GameObject m_floorChipPrefab;

	// Use this for initialization
	void Start () {
		float size = 246.0f/100f;
		for(int i=0;i<3;i++){
			for(int k=0;k<3;k++){
				var chipObj = Instantiate(m_floorChipPrefab) as GameObject;
				chipObj.transform.parent = transform;
				chipObj.transform.localPosition = new Vector3(i*size,k*size,0);
				chipObj.transform.localScale = Vector3.one;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
