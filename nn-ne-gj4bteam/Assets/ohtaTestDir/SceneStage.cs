﻿using UnityEngine;
using System.Collections;

public class SceneStage : MonoBehaviour {

	[SerializeField] GameObject	m_stageMapPrafab;
	[SerializeField] GameObject	m_charaPrefab;


	// Use this for initialization
	void Start () {
		var stageObject = Instantiate(m_stageMapPrafab) as GameObject;
		stageObject.transform.parent = transform;
		stageObject.transform.localPosition = m_stageMapPrafab.transform.localPosition;
		stageObject.transform.localScale = Vector3.one;

		var charaObject = Instantiate(m_charaPrefab) as GameObject;
		charaObject.transform.parent = transform;
		charaObject.transform.localPosition = Vector3.zero;
		charaObject.transform.localScale = m_charaPrefab.transform.localScale;
		charaObject.GetComponent<Character>().MoveTo(new Vector3(1,1,1));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
