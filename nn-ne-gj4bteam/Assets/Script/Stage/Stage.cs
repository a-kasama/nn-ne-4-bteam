﻿using UnityEngine;
using System.Collections;

public class Stage : MonoBehaviour {

	/*    0 1 2 x
	 *  0 0 3 6
	 *  1 1 4 7 
	 *  2 2 5 8 
	 *  y
	 */
	/// <summary>
	/// The m_floor object.
	/// </summary>
	[SerializeField] GameObject[]	m_floorObject;
	[SerializeField] int			m_width;
	[SerializeField] int			m_height;


	public Vector3 GetFloorPosition(int x,int y){
		int index = y + x * (m_height);
		return m_floorObject[index].transform.localPosition;
	}
	
	public int GetSize(){
		return m_width * m_height;
	}

	public int GetWidth(){
		return m_width;
	}

	public int GetHeight(){
		return m_height;
	}


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
