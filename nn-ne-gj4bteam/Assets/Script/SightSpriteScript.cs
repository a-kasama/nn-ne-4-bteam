﻿using UnityEngine;
using System.Collections;

public class SightSpriteScript : MonoBehaviour {

	SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start ()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		spriteRenderer.enabled = false;
	}

	public void SetUpImage(float position_x, float position_y)
	{
		spriteRenderer.enabled = true;
		transform.position = new Vector3(position_x, position_y, 0.0f);
	}

	public void HideImage()
	{
		spriteRenderer.enabled = false;
	}
}
