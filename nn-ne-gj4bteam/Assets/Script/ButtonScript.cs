﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
	//スプライト
	public Sprite JumpSprite;
	public Sprite DownSprite;
	public Sprite MeowSprite;

	//Imageコンポーネント
	private Image images;

	void Start()
	{
		images = gameObject.GetComponent<Image>();
	}
	
	public void ButtonOnClick ()
	{
		InGameManager.getInstance ().TapActionButton ();
	}

	public void ChangeButtonImage(int changeType)
	{
		switch (changeType)
		{
		case 0:
			Debug.Log ("ジャンプに変化するにゃ！");
			images.sprite = JumpSprite;
			break;
		case 1:
			Debug.Log ("降りるに変化するにゃ！");
			images.sprite = DownSprite;
			break;
		case 2:
			Debug.Log ("にゃーに変化するにゃ！");
			images.sprite = MeowSprite;
			break;
		default:
			break;
		}
	}
}
