﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyCharacter : CharacterBase {


	public delegate void Task();

	[SerializeField] GameObject	m_startObject;

	Task m_task;
	float m_length;
	float m_time, m_totalTime;
	Vector3 m_movedir;
	float m_movelength = 2.56f;

	public enum dir {
		Down,
		Left,
		Right,
		Up,
	}

	dir m_dir;

	List<dir> m_dirList = new List<dir>();

	void DoMove(){
		var rand = UnityEngine.Random.Range(0,m_dirList.Count-1);
		var dir = m_dirList[rand];


		Vector3 topos = Vector3.zero;
		if(dir==dir.Down){
			topos += new Vector3(0,-m_movelength,0);
		}
		else if(dir==dir.Up){
			topos += new Vector3(0,m_movelength,0);
		}
		else if(dir==dir.Left){
			topos += new Vector3(-1*m_movelength,0,0);
		}
		else if(dir==dir.Right){
			topos += new Vector3(m_movelength,0,0);
		}

		MoveTo(topos,2.0f);
	}

	public void MoveTo(Vector3 toPosition, float time = 1.0f){
		m_totalTime = time;
		m_time = time + Time.realtimeSinceStartup;
		m_length = Vector3.Distance(toPosition, transform.localPosition);
		m_movedir = (toPosition - transform.localPosition)/m_length;
		m_task = moveCore;
	}
	
	void moveCore() {
		transform.localPosition += m_movedir * m_length * (Time.deltaTime/(m_totalTime));
		if(Time.realtimeSinceStartup >= m_time){
			moveFinish();
			m_task = null;
		}
	}

	void moveFinish(){

		Ray ray = Camera.main.ScreenPointToRay(transform.localPosition);
		RaycastHit hit = new RaycastHit();

		if (Physics.Raycast(ray, out hit, 10)) {
			GameObject selectedGameObject = hit.collider.gameObject;
			if(selectedGameObject.GetComponent<StagePointer>()!=null){

			}

		}
	}



	void GetMoveDirList(GameObject obj){
		m_dirList.Clear();
		if(obj.GetComponent<StagePointer>().MoveDown){
			m_dirList.Add (dir.Down);
		}
		if(obj.GetComponent<StagePointer>().MoveLeft){
			m_dirList.Add (dir.Left);
		}
		if(obj.GetComponent<StagePointer>().MoveUp){
			m_dirList.Add (dir.Up);
		}
		if(obj.GetComponent<StagePointer>().MoveRight){
			m_dirList.Add (dir.Right);
		}
	}

	// Use this for initialization
	protected override void Start () {
		transform.localPosition = m_startObject.transform.localPosition;
		GetMoveDirList(m_startObject);
	}
	
	// Update is called once per frame
	protected override void Update () {
	
	}
}
