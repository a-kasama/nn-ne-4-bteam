﻿using UnityEngine;
using System.Collections;

public class Character : CharacterBase {

	public delegate void Task();

	Task m_task;
	float m_length;
	float m_time, m_totalTime;
	Vector3 m_dir;

	public void MoveTo(Vector3 toPosition, float time = 1.0f){
		m_totalTime = time;
		m_time = time + Time.realtimeSinceStartup;
		m_length = Vector3.Distance(toPosition, transform.localPosition);
		m_dir = (toPosition - transform.localPosition)/m_length;
		m_task = moveCore;
	}

	void moveCore() {
		transform.localPosition += m_dir * m_length * (Time.deltaTime/(m_totalTime));
		if(Time.realtimeSinceStartup >= m_time){
			m_task = null;
		}
	}

	// Use this for initialization
	protected override void Start () {

	}
	
	// Update is called once per frame
	protected override void Update () {
		if(m_task!=null){
			m_task();
		}
	}
}
