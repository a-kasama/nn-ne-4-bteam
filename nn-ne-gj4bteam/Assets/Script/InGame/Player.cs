﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	public bool isMove = true;
	public bool isTenjo = false;
	public Character _Character;
	public GameObject _sprite;
	public StagePointer currentPointer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter(Collider collision)
	{
		Debug.Log("onTrriger");

		if (collision.gameObject.tag == "enemy") {
			InGameManager.getInstance().GameEnd();
		}

		if (collision.gameObject.tag == "pointer") {
			currentPointer = collision.gameObject.GetComponent<StagePointer>();
			Debug.Log("onTrrigerPointer");
		}
	}

}
