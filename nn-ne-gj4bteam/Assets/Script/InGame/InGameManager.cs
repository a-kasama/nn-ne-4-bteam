﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class InGameManager : MonoBehaviour
{
	public string backScene;
	static InGameManager _inGameManager;
	public Player _Player;
	//public GameObject _StagePointerBox;
	//public List<StagePointer> _StagePointerList;
	float ignoreInputTimer = 0;
	public Text GameClearText;
	public Text GameOverText;
	public Image GameClearImage;
	public Image GameOverImage;
	public Image AttackImage;
	public Image AttackImageBg;
	StagePointer currentStagePointer;
	public ButtonScript _ButtonScript;
	//player class
	//enemy  class
	//public List<Enemy> _EnemyList;

	
	// Use this for initialization
	void Awake ()
	{
		_inGameManager = this;
	}

	static public InGameManager getInstance ()
	{
		return _inGameManager;
	}

	void Start ()
	{
		GameStart ();
	}

	// Update is called once per frame
	void Update ()
	{
		//Touch touch = Input.GetTouch(0);
		//Vector3 touchPos = touch.position;
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			//Ray ray = Camera.main.ScreenPointToRay(touchPos);
			RaycastHit hit = new RaycastHit ();
			
			if (Physics.Raycast (ray, out hit)) {
				GameObject obj = hit.collider.gameObject;
				if (obj.tag == "pointer") {
					if (!_Player.isTenjo) {
						float dX = obj.transform.position.x - _Player._Character.transform.position.x;
						float dY = obj.transform.position.y - _Player._Character.transform.position.y;

						Vector3 vec = new Vector3 (obj.transform.localPosition.x, obj.transform.localPosition.y, 0);
						if (Mathf.Abs (dX) > Mathf.Abs (dY)) {
							if (dX > 0 && dX < 2 && _Player.currentPointer.MoveRight) {
								moveTo (vec.x, _Player._Character.transform.localPosition.y);
							} else if (dX < 0 && dX > -2 && _Player.currentPointer.MoveLeft) {
								moveTo (vec.x, _Player._Character.transform.localPosition.y);
							}
						} else {
							if (dY > 0 && dY < 2 && _Player.currentPointer.MoveUp) {
								moveTo (_Player._Character.transform.localPosition.x, vec.y);
							} else if (dY < 0 && dY > -2 && _Player.currentPointer.MoveDown) {
								moveTo (_Player._Character.transform.localPosition.x, vec.y);
							}
						}

					}
				}
				//Debug.Log(obj.name);
			}
		}

		ignoreInputTimer -= Time.deltaTime;
	}

	void moveTo (float x, float y)
	{
		if (ignoreInputTimer <= 0) {
			//Vector3 vec = new Vector3( 0,0, 0);
			Vector3 vec = new Vector3 (x, y, 0);

			_Player._Character.MoveTo (vec);
			ignoreInputTimer = 1.1f;
		}
	}
	
	public void GameStart ()
	{

	}

	public void CreateStage ()
	{
		
	}

	public void GameClear ()
	{
		GameClearImage.enabled = true;
	}

	public void GameEnd ()
	{
		GameOverImage.enabled = true;
	}

	public void BackScene ()
	{
		Application.LoadLevel (backScene);
	}

	public void TapActionButton ()
	{
		if (ignoreInputTimer <= 0) {
			Debug.Log ("Button Push !!");
			if (!_Player.isTenjo) {
			
				_Player.isTenjo = true;
				_Player._sprite.gameObject.GetComponent<SpriteRenderer> ().color = new Color (1.0f, 1.0f, 1.0f, 0.2f);
				//_Player._Character.transform.position = new Vector3 ();
				_ButtonScript.ChangeButtonImage (1);
			} else {
				StartCoroutine ("AttackIEnumerator");
				_Player._sprite.gameObject.GetComponent<SpriteRenderer> ().color = new Color (1.0f, 1.0f, 1.0f, 1.0f);
				_ButtonScript.ChangeButtonImage (0);
				_Player.isTenjo = false;
			}
		}
	}

	public void FocusFog ()
	{

	}

	public void CheckRooms ()
	{

	}

	public void playSound ()
	{

	}

	//StartCoroutine ("AttackIEnumerator");を呼び出せばいい
	public IEnumerator AttackIEnumerator ()
	{
		Debug.Log ("アタックにゃ！");
		AttackImage.enabled = true;
		AttackImageBg.enabled = true;

		yield return new WaitForSeconds (0.5f);

		AttackImage.enabled = false;
		AttackImageBg.enabled = false;
	}
}
